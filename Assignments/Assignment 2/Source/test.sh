#!/bin/bash

# Alec Sanchez
# CST-221-TR700A
# September 9, 2020
# This is my own work.

PWD=$(pwd)

RED='\033[0;31m'
BLUE='\033[0;34m'

LBLUE='\033[1;34m'
LPURPLE='\033[1;35m'
LCYAN='\033[1;36m'
NC='\033[0m'

find "$PWD" -print0 | while read -d $'\0' file
do
	printf "${LBLUE}${file##*/}${NC}\n"
	echo -n "Is directory: "

	if [ -d "$file" ]; then
		printf "${BLUE}TRUE${NC}\n"
	else
		printf "${RED}FALSE${NC}\n"

		echo

		printf "Head: ${LPURPLE}\n"
		head -n 5 "$file"

		printf "${NC}\n"

		printf "Tail: ${LCYAN}\n"
		tail -n 5 "$file"
	fi 

	printf "${NC}\n"
	echo "----------------------------------------------------------------------------"
	echo
done

touch "IveBeenTouched.txt"
