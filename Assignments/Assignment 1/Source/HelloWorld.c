// Alec Sanchez
// CST-221-TR700A
// September 7, 2020
// This is based off of the required code in the assignment.

// Topic 1: Basic Operating Systems and Shell Programming
// Systems and Tools
//
// Hello World in C
// September 7, 2020

#include<stdio.h>

int main () {
    printf("Hello World!\n");
}
